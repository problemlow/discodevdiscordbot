from inspect import currentframe, getframeinfo  # I have never used this library before
from discord import commands  # I have never used this library before in my entire life, my research told me i needed this library to make a bot for discord
from operator import itemgetter  # I have never used this library before in my entire life
import asyncio  # I have never used this library before in my entire life, my research showed me i needed this of a similar library to effectivly use the discord library
import errno  # I have never used this library before in my entire life, my research told me this was useful for reading error information
import csv
import os


# #################### - Settings Start - ####################
startup_extensions = ["Music"]
controlCharacter = "!"
key = "NDUwODUzNzYwMTE0MDk4MjE2.Dn1r6g.hjKPivkbLz1n-bR9R37eJ9rZoJ8"
# ##################### - Settings End - #####################


bot = commands.Bot(controlCharacter)  # this tells the bot when it's apporpriate to take message context


@bot.event
async def on_ready():  # This function is run upon the bots startup completing
    os.system('cls')
    print("Name: {}".format(bot.user.name))
    print("ID: {}".format(bot.user.id))
    print("Status: Running")
    print("########################################")


class Main_Commands():
    def __init__(self, bot):
        self.bot = bot


@bot.command(pass_context=True)  # This line ensures that the function defined on the next has the context of the server, channel, command issuer and all related information so as to allow it to analyze the situation at hand and respond accordingly
async def ping(ctx):  # This function just defines a simple one line command that can be used to check if the bot is working or not
    """Returns pong to show server acknowledgement"""
    await bot.say("Pong")


@bot.command(pass_context=True)  # This line ensures that the function defined on the next has the context of the server, channel, command issuer and all related information so as to allow it to analyze the situation at hand and respond accordingly
async def credits(ctx):  # This function is a shameless plug for my freelancer page
    """A list of credits for the bot"""
    await bot.say("Hire the developer of this bot for your own project https://www.freelancer.co.uk/u/Salcarsay")


@bot.command(pass_context=True)  # This line ensures that the function defined on the next has the context of the server, channel, command issuer and all related information so as to allow it to analyze the situation at hand and respond accordingly
async def motd(ctx):
    """Allows users with access to assign remove and view all auto messages"""
    errorI = "```You've issued the MOTD command with incorrect parameters\nUsage example:\n!motd mode, message, delay\n      set            minutes\n      remove\n      display```"
    errorN = "```You've issued the MOTD command with no parameters\nUsage example:\n!motd mode, message, delay\n      set            minutes\n      remove\n      display```"
    # errorI = "```You've issued the MOTD command with incorrect parameters\nUsage example:\n!motd mode      message      delay\n      set                    minutes\n      remove\n      display```"
    # errorN = "```You've issued the MOTD command with no parameters\nUsage example:\n!motd mode      message      delay\n      set                    minutes\n      remove\n      display```"
    command = ctx.message.content
    strip = command[6:]
    temp = strip.split(",")
    print("motd - temp", temp)
    if len(strip) == 0 or strip == None:
        await bot.say(errorN)
        return  # This line ends the function returning nothing
    temp2 = []  # lines like this are for assignment of empty arrays to be used later in the program
    if len(temp) != 0:
        for thing in temp:
            if thing != None and len(thing) != 0:
                temp2.append(thing)
    if str(temp[0]).lower() != "display" and str(temp[0]).lower() != "remove":
        try:
            temp2[2] = int(temp2[2])
            if temp2[2] < 1:
                return
        except:
            await bot.say(str("You must specify minutes/remove as a positive integer" + errorN))
            return  # This line ends the function returning nothing
        if temp2[0].lower() == "set" or temp2[0].lower() == "remove" or temp2[0] == "display":
            pass
        else:
            await bot.say("The mode parameter must be either 'Set', 'Remove' or 'Display' " + errorN)
            return  # This line ends the function returning nothing
        if temp2[0].lower() == "set":
            await setDataMotd(ctx.message.server, ctx.message.channel, temp2[1], temp2[2], False)
            await bot.say("Sucessfully added message "+str(temp2[1])+" to the auto message system with a delay of "+str(temp2[2]))
    elif temp2[0].lower() == "display":
        try:
            motdData = await getDataMotd(ctx.message.server, "motd")
        except IndexError:
            print("motd - No auto message data found for server", str(ctx.message.server.name), str(ctx.message.server.id))
            return  # This line ends the function returning nothing
        temp = "```ID  Channel  Message  Delay\n"
        iterator = 0
        for message in motdData:
            iterator += 1
            temp += str(iterator)+" "+str(bot.get_channel(message[0]).name)+" "+str(message[1])+" "+str(float(message[2])/60)+"\n"
        await bot.say(str(temp+"```"))
    elif temp2[0].lower() == "remove":
        error = "When using the remove function you must specify which message to remove as a positive integer as can be found my issuing the motd display command."
        try:
            number = int(temp2[1])
        except:
            await bot.say(error)
            return  # This line ends the function returning nothing
        if number < 1:
            await bot.say(error)
            return  # This line ends the function returning nothing
        motdData = await getData(ctx.message.server, "motd")
        iterator = 0
        for entry in motdData:
            if len(entry) == 0:
                del motdData[iterator]
            else:
                entry[2] = int(round(float(entry[2])))
            iterator += 1
        motdData.sort(key=itemgetter(2))
        if number >= 1 and number <= len(motdData):
            del motdData[int(number)-1]
            # write motd data to motd.csv excluding the removed entry
            os.remove("servers/"+str(ctx.message.server.id)+"/motd.csv")
            for messageData in motdData:
                await setDataMotd(ctx.message.server, bot.get_channel(messageData[0]), str(messageData[1]), str(int(round(float(messageData[2])/60))), False)
            await bot.say("Sucessfully removed message "+str(number)+" from the auto message system")
        elif number > len(motdData):
            await bot.say(error)
            return  # This line ends the function returning nothing


@bot.command(pass_context=True)  # This line ensures that the function defined on the next has the context of the server, channel, command issuer and all related information so as to allow it to analyze the situation at hand and respond accordingly
async def prune(ctx, number):
    """Prunes the specified number of messages from the chat."""
    # this is the error messaage displayed when anything goes wrong
    if ctx.message.author.server_permissions.manage_messages != True:
        await bot.say("In order to use this command you must be a member of a role with the 'Manage Messages' permission")
        return  # This line ends the function returning nothing
    error = "Try again valid range is a whole number from 2-99."
    messages = []  # lines like this are for assignment of empty arrays to be used later in the program
    try:
        number = int(number)
    except:
        await bot.say(error)
        return  # This line ends the function returning nothing
    if number >= 2 and number <= 99 and isinstance(number, int) == True:
        async for message in bot.logs_from(ctx.message.channel, limit=number+1):
            messages.append(message)
        await bot.delete_messages(messages)
    else:
        await bot.say(error)


@bot.command(pass_context=True)  # This line ensures that the function defined on the next has the context of the server, channel, command issuer and all related information so as to allow it to analyze the situation at hand and respond accordingly
async def perms(ctx):
    """Displays the users permissions on the server"""
    text = ""
    for thing in ctx.message.author.server_permissions:
        text += str(thing[0])+" - "+str(thing[1])+"\n"
    await bot.say("```"+text+"```")


@bot.command(pass_context=True)  # This line ensures that the function defined on the next has the context of the server, channel, command issuer and all related information so as to allow it to analyze the situation at hand and respond accordingly
async def roles(ctx):
    """Displays all the servers roles complete with id's and number of members of the total"""
    roles = []  # lines like this are for assignment of empty arrays to be used later in the program
    for role in ctx.message.server.roles:
        roles.append([str(role), 0, role.id])
    for member in ctx.message.server.members:
        for sRole in range(len(ctx.message.server.roles)):
            for mRole in member.roles:
                if ctx.message.server.roles[sRole] == mRole:
                    roles[sRole][1] += 1
    # (string[2:])[:-2]
    message = "```Role Name                          Number of Members    Role ID\n"
    for role in range(len(roles)):
        if str(roles[role][0]) != "@everyone":
            adition0 = str(roles[role][0])
            adition1 = str(roles[role][1])+"/"+str(roles[0][1])
            adition2 = str(roles[role][2])
            text = adition0
            while len(text) != 35:
                text += " "
            text += adition1
            while len(text) != 56:
                text += " "
            text += adition2+"\n"
            message += text
        else:
            text = "#"
            while len(text) < 74:
                text += "#"
            message += text+"\n"
    await bot.say(message+"```")


@bot.command(pass_context=True)  # This line ensures that the function defined on the next has the context of the server, channel, command issuer and all related information so as to allow it to analyze the situation at hand and respond accordingly
async def addrole(ctx):
    """Allows authorized users to add roles that can then be automatically assigned to users who request them via usage of the role command"""
    server = ctx.message.server
    desiredRole = ctx.message.content[9:]
    roleToAdd = None
    if ctx.message.server.owner.id == ctx.message.author.id:
        if len(desiredRole) >= 1:
            for role in server.roles:
                if str(role).lower() == desiredRole.lower():
                    roleToAdd = role
                    break
        if roleToAdd == None:
            await bot.say("That is an invalid role")
            return  # This line ends the function returning nothing
        # Get assigned roles
        rolesArray = await getData(server, "assignableRoles")
        if rolesArray == "Failed":
            await bot.say("No permissions file can be located creating one with specified role")

        # Make sure the role isnt already requestable by users
        if len(rolesArray) > 0 and roleToAdd != None:
            for role in rolesArray:
                if str(role) == str(roleToAdd.id):
                    await bot.say("You are already a member of: " + str(roleToAdd))
                    return  # This line ends the function returning nothing

        # Check if the desired role is allowed to be assigned to the user
        for role in rolesArray:
            if (str(role)[2:])[:-2].lower() == desiredRole.lower():
                desiredRole = role.id
                break
        await setDataRoles(server, roleToAdd.id, str(roleToAdd), "assignableRoles")
    else:
        await bot.say("Only the server owner and users inside assigned operator roles can issue this command")


@bot.command(pass_context=True)  # This line ensures that the function defined on the next has the context of the server, channel, command issuer and all related information so as to allow it to analyze the situation at hand and respond accordingly
async def role(ctx):
    """Allows you to request to join a role by typing '!role roleName'"""
    bot.delete_message(ctx.message)
    roleToAssign = None
    server = ctx.message.server
    author = ctx.message.author
    desiredRole = ctx.message.content[6:]

    # Get the assignable roles
    rolesArray = await getData(server, "assignableRoles")
    if rolesArray == "Failed":
        return  # This line ends the function returning nothing

    # await bot.say(server)
    # Make sure the user actually specified a role and that role is a real role in the server
    if len(desiredRole) >= 1:
        for role in server.roles:
            if str(role).lower() == desiredRole.lower():
                roleToAssign = role
                break

        # Make sure the user doesnt already have the role they are requesting
        for role in author.roles:
            if roleToAssign != None:
                if str(role).lower() == str(roleToAssign).lower():
                    await bot.say("You are already a member of: " + str(role))
                    return  # This line ends the function returning nothing

        # Check if the desired role is allowed to be assigned to the user
        assignable = False
        for role in rolesArray:
            try:
                if (str(role)[2:])[:-2] == str(roleToAssign.id):
                    assignable = True
                    break
            except AttributeError:
                await bot.say("That role does not exist")
                return

        # This notifies the user that the bot cannot assign this role to them
        if assignable == False and roleToAssign != None:
            message = "```"
            for role in server.roles:
                if str(role) == "@everyone":
                    pass
                else:
                    hash = False
                    for assignableRole in rolesArray:
                        if str(role.id) == (str(assignableRole)[2:])[:-2]:
                            hash = True
                    if hash == True:
                        message += "\n#"+str(role)
                    else:
                        message += "\n"+str(role)
            await bot.say("I'm afraid I dont have permission to assign this role to you.\nPlease specify your desired role from the following options with a '#' character next to them:\n"+message+"```")

        # This notifies the user that they have selected a role that does not exist
        if assignable == False and desiredRole != None and roleToAssign == None:
            message = "```"
            for role in server.roles:
                if str(role) == "@everyone":
                    pass
                else:
                    hash = False
                    for assignableRole in rolesArray:
                        if str(role.id) == (str(assignableRole)[2:])[:-2]:
                            hash = True
                    if hash == True:
                        message += "\n#"+str(role)
                    else:
                        message += "\n"+str(role)
            await bot.say("I'm afraid the role '"+desiredRole+"' does not exist on this server.\nPlease specify your desired role from the following options with a '#' character next to them:\n"+message+"```")

        # If the user doesnt already have the role they requested and it is in the list of assignable roles this gives it to them
        if roleToAssign != None and assignable == True:
            await bot.add_roles(author, roleToAssign)
            text = str("Added role '" + str(roleToAssign) + "' To user: " + str(author)[:-5])
            await bot.say(text)
    else:
        message = "```"
        for role in server.roles:
            if str(role) == "@everyone":
                pass
            else:
                hash = False
                for assignableRole in rolesArray:
                    if str(role.id) == (str(assignableRole)[2:])[:-2]:
                        hash = True
                if hash == True:
                    message += "\n#"+str(role)
                else:
                    message += "\n"+str(role)
        await bot.say("Please specify your desired role from the following options with the '#' character next to them:\n"+message+"```")


async def streamingCheck():
    while not bot.is_closed:
        try:
            await bot.wait_until_ready()
            while True:
                servers = os.listdir("Servers")
                # os.system('cls')
                # print("Name: {}".format(bot.user.name))
                # print("ID: {}".format(bot.user.id))
                # print("Status: Running")
                # print("########################################")
                # print("0%   - Streaming Check")
                for server in range(len(servers)):
                    server = servers[server]
                    server = bot.get_server(server)
                    temp = await getData(server, "streamingRole")
                    if temp == "Failed" or temp == None:
                        pass
                    else:
                        # print("     - Streaming Check for server:", str(server))
                        streamingRole = None
                        for role in server.roles:
                            if role.id == ((str(temp)[3:])[:-3]):
                                streamingRole = ((str(temp)[3:])[:-3])
                                break
                        exists = False
                        for role in server.roles:
                            if str(role.id) == str(streamingRole):
                                streamingRole = role
                                exists = True
                                break
                        if exists == False:
                            break
                    # print("15%  - Streaming Check")
                    for member in server.members:
                        if member.game != None and int(member.game.type) == 1 and streamingRole != None:
                            add = True
                            for role in member.roles:
                                print(role.id)
                                if int(role.id) == int(streamingRole.id):
                                    add = False
                                    break
                            if add == True:
                                await bot.add_roles(member, streamingRole)
                                print("Added memeber:", str(member), "role to", str(streamingRole))
                        elif member.game != None and member.game.type != 1 and streamingRole != None:
                            for role in member.roles:
                                if role.id == streamingRole.id:
                                    await bot.remove_roles(member, streamingRole)
                                    print("Removed role", str(streamingRole.id), "from", str(member))
                                    break
                        elif member.game == None and streamingRole != None:
                            await bot.remove_roles(member, streamingRole)
                    print("100% - Streaming Check: Completed")
        except ConnectionResetError:
            print("ConnectionResetError - This is fine, but if it keeps happening please check your avaliable bandwidth")
        except Exception as error:
            print("streamingCheck - Warning Unhandled exception Internal Server Error is on Discords end", error)


# Helper Functions
async def getData(server, type):
    array = []  # lines like this are for assignment of empty arrays to be used later in the program
    try:
        if type == "motdLoop":
            with open("servers/"+str(server)+"/"+type+".csv", "r") as file:
                array = list(csv.reader(file))
        else:
            with open("servers/"+str(server.id)+"/"+type+".csv", "r") as file:
                array = list(csv.reader(file))

    except FileNotFoundError:
        try:
            if type == "motdLoop":
                pass
            else:
                await bot.say("No settings for this server can be located, please alert the server owner to set these up")
                print("getData - Failed", "line", getframeinfo(currentframe()).lineno, "type:", type)
                return("Failed")
        except Exception as error:
            if type == "streamingRole":
                return("Failed")
            else:
                print("getData - Error:", str(error), "line:", getframeinfo(currentframe()).lineno, "type:", type)
                return("Failed")
    except Exception as error:
        print("getData - Error:", str(error), "line", getframeinfo(currentframe()).lineno, "type:", type)
        return("Failed")
    return(array)


async def getData2(server, type):
    array = []  # lines like this are for assignment of empty arrays to be used later in the program
    try:
        if type == "motd":
            with open("servers/"+str(server)+"/"+type+".csv", "r") as file:
                array = list(csv.reader(file))
        else:
            with open("servers/"+str(server.id)+"/"+type+".csv", "r") as file:
                array = list(csv.reader(file))

    except FileNotFoundError:
        try:
            if type == "motd":
                array = "Failed"
            else:
                await bot.say("No settings for this server can be located, please alert the server owner to set these up")
                print("getData2 - Failed", "line", getframeinfo(currentframe()).lineno, "type:", type)
                return("Failed")
        except Exception as error:
            print("getData2 - Error:", str(error), "line", getframeinfo(currentframe()).lineno, "type:", type)
            return("Failed")
    except Exception as error:
        print("getData2 - Error:", str(error), "line", getframeinfo(currentframe()).lineno, "type:", type)
        return("Failed")
    return(array)


async def setDataRoles(server, roleToAdd, desiredRole, type):  # takes the server pointer and a roles id
    array = []  # lines like this are for assignment of empty arrays to be used later in the program
    try:
        with open("servers/"+str(server.id)+"/"+type+".csv", "r") as file:
            array = list(csv.reader(file))
    except FileNotFoundError:
        try:
            os.makedirs("servers/"+str(server.id))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        f = open("servers/"+str(server.id)+"/"+type+".csv", "w+")  # this is here to trigger the file not found error if there is not a file where it's supposed to be so as to allow the program to provide the correct error message
        f.close()
    alreadyInList = False
    for role in array:
        if (str(role)[2:])[:-2] == str(roleToAdd):
            alreadyInList = True
            break
    if alreadyInList == False:
        array.append(roleToAdd)
        with open("servers/"+str(server.id)+"/"+type+".csv", "a") as file:
            file.write(str(roleToAdd)+"\n")
        await bot.say(str("Sucessfully added role "+str(desiredRole).capitalize()+" to joinable list"))
        return(True)
    else:
        await bot.say("Role "+desiredRole+" is already assignable")


async def setDataMotd(server, channel, message, delay, alert):  # takes the server pointer and a message as well as a delay
    array = []  # lines like this are for assignment of empty arrays to be used later in the program
    try:
        with open("servers/"+str(server.id)+"/motd.csv", "r") as file:
            array = list(csv.reader(file))  # ignore the not used error this is required
    except FileNotFoundError:
        try:
            os.makedirs("servers/"+str(server.id))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    with open("servers/"+str(server.id)+"/motd.csv", "a") as file:
        file.write(str(channel.id)+","+str(message).capitalize()+","+str(float(delay)*60)+"\n")
    if alert == True:
        await bot.say(str("Sucessfully added message: "+str(message).capitalize()+" to auto message list with a delay of "+str(delay)+" minutes"))


async def getDataMotd2(server):
    motdData = await getData2(server, "motd")
    iterator = 0
    for entry in motdData:
        if len(entry) == 0:
            del motdData[iterator]
        else:
            try:
                entry[2] = int(round(float(entry[2])))
            except:
                return("Failed")
        iterator += 1
    motdData.sort(key=itemgetter(2))
    return(motdData)


async def getDataMotd(server, mode):
    motdData = await getData(server, mode)
    iterator = 0
    for entry in motdData:
        if len(entry) == 0:
            del motdData[iterator]
        else:
            entry[2] = int(round(float(entry[2])))
        iterator += 1
    motdData.sort(key=itemgetter(2))
    return(motdData)


async def motdLoopOld():
    await bot.wait_until_ready()
    while not bot.is_closed:
        servers = os.listdir("Servers")
        for server in servers:
            messages = await getDataMotd2(server)
            if messages == "Failed":
                pass
            else:
                for message in messages:
                    print("motdLoop -", str(server), str(message[0]), str(message[1]))
                    try:
                        channel = bot.get_channel(str(message[0]))
                        await bot.send_message(channel, str(message[1]))
                        await asyncio.sleep(int(message[2]))
                    except:
                        print("MOTD Loop - Some sort of error occured perhaps discords servers went down and I never got a responce back? Server:", server, "channel:", channel, "message:", message[1])
                        await asyncio.sleep(5)


async def motdSetup():
    print("motdSetup: Started")
    global motdMessages
    await bot.wait_until_ready()
    print("motdSetup: Bot Online Confirmed")
    servers = os.listdir("Servers")
    for server in servers:
        messages = await getDataMotd2(server)
        if messages == "Failed":
            pass
        else:
            for message in messages:
                try:
                    motdMessages.append([str(message[0]), str(message[1]), int(message[2]), False])
                except:
                    print("MOTD Loop - Some sort of error occured perhaps discords servers went down and I never got a responce back? Server:", server, "channel:", str(message[0]), "message:", message[1])
    print("motdSetup: Finished")
    return


async def motdUpdate():
    print("motdUpdater: Started")
    global motdMessages
    await bot.wait_until_ready()
    print("motdUpdate: Bot Online Confirmed")
    while not bot.is_closed:
        servers = os.listdir("Servers")
        print("motdUpdater: Running")
        for server in servers:
            messages = await getDataMotd2(server)
            if messages == "Failed":
                pass
            else:
                for message in messages:
                    exists = False
                    for motdMessage in motdMessages:
                        channel = str(message[0])
                        print(channel, motdMessage[0], message[1], motdMessage[1], message[2], motdMessage[2])
                        if str(channel) == str(motdMessage[0]) and str(message[1]) == str(motdMessage[1]) and str(message[2]) == str(motdMessage[2]):
                            # print("EXISTS############################")
                            exists = True
                    try:
                        if exists == False:
                            motdMessages.append([message[0], message[1], message[2], False])
                    except Exception as error:
                        print("motdUpdate - error:```", error, "```")
        for thing in motdMessages:
            # pass
            print("motdUpdate -", thing)
        await asyncio.sleep(10)


async def motdThreadSpawnner():
    print("motdThreadSpawnner: Started")
    await bot.wait_until_ready()
    print("motdThreadSpawnner: Bot Online Confirmed")
    global motdMessages
    while not bot.is_closed:
        for motdMessage in motdMessages:
            if motdMessage[3] == False:
                print("motdThreadSpawnner: Spawnning new message thread")
                bot.loop.create_task(motdSendMessage())
                await asyncio.sleep(.5)
                print("motdThreadSpawnner: Spawned new message thread")
        await asyncio.sleep(1)


async def motdSendMessage():
    print("motdSendMessage: Started - Locating message data")
    await bot.wait_until_ready()
    print("motdSendMessage: Bot Online Confirmed")
    global motdMessages
    for message in motdMessages:
        if message[3] == False:
            message[3] = True
            channel = message[0]
            text = message[1]
            delay = message[2]
            print("motdSendMessage: Message Data Located running the following:")
            print("motdSendMessage: Channel", str(channel), "Message:", message[1], "Delay:", str(delay))
            break
    channel = bot.get_channel(str(message[0]))
    channelID = message[0]
    while not bot.is_closed:
        exists = False
        for message in motdMessages:  # (string[2:])[:-2]
            # print(str(message[0]), str(channelID), "\n", str(message[1]), (str(text)[2:])[:-2], "\n", str(message[2]), (str(delay)[1:])[:-1])
            if str(message[0]) == str(channelID) and str(message[1]) == str(text) and str(message[2]) == str(delay):
                exists = True
                break
        if exists == False:
            print("Message no longer exists self terminating message thread.")
            break  # this ends the loop because the auto message has been removed from the list
        else:
            try:
                await bot.send_message(channel, str(text))
                await asyncio.sleep(delay)
            except Exception as error:
                print("motdSendMessage - Tried to send message but got:")
                print("motdSendMessage -", error)
                await asyncio.sleep(5)

if __name__ == "__main__":
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = "{}: {}".format(type(e).__name__, e)
            print("cogLoader - Failed to load extension {}\n{}".format(extension, exc))

motdMessages = []  # this is used as a global variable in the motdSetup function
bot.loop.create_task(motdSetup())
bot.loop.create_task(motdUpdate())
bot.loop.create_task(motdThreadSpawnner())
bot.loop.create_task(streamingCheck())
# FOR SERVER IN SERVERS:
#   bot.loop.create_task(motdLoop(server))
#   bot.loop.create_task(streamingCheck(server))
bot.run(key)
